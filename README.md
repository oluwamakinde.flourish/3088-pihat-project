This HAT is compliant with the microHAT requirements of the Raspberry Pi and contains a temperature sensor that monitors the temperature in an environment and stores the values it detects on the EEPROM of the HAT (memory).

How you can contribute to this project:
- Making changes to the requirements for the HAT by adding more sensors to the HAT
- Improving on the design of the HAT
- Improving on the build of the HAT and how it can be applied to real-life situations
